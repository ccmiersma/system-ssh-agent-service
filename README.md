System SSH Agent Service
========================

This service allows you to start the ssh-agent for host or user keys and offload security from other `systemd` services
so that they can reliably use the system's private ssh keys rather than user keys. The primary benefit is the flexibility of managing keys.
It does not increase/decrease security of the system's already unencrypted keys.
