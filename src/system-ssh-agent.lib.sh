SYSTEM_SSH_KEYS=${SYSTEM_SSH_KEYS:-"/etc/ssh/ssh_host_ecdsa_key /etc/ssh/ssh_host_ed25519_key  /etc/ssh/ssh_host_rsa_key"}
SYSTEM_SSH_AGENT_ENV_FILE=${SYSTEM_SSH_AGENT_ENV_FILE:-/tmp/system-ssh-agent-env.sh}

export SYSTEM_SSH_KEYS
export SYSTEM_SSH_AGENT_ENV_FILE

function _ssh_agent_start(){
  ssh-agent -s > $SYSTEM_SSH_AGENT_ENV_FILE
}

function _ssh_agent_stop(){
  ssh-agent -k
}

function _ssh_agent_load(){
  source $SYSTEM_SSH_AGENT_ENV_FILE
}

function _ssh_add_keys(){
  for key in $SYSTEM_SSH_KEYS
  do
    ssh-add $key
  done
}


# vim: syntax=sh
