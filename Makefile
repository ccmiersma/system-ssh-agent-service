all: build



build:  src/system-ssh-agent.sh.erb src/system-ssh-agent.service.erb docs/system-ssh-agent.1.md
	mkdir build
	erb -r ./build-config.rb src/system-ssh-agent.sh.erb > build/system-ssh-agent.sh
	erb -r ./build-config.rb src/system-ssh-agent.service.erb > build/system-ssh-agent.service
	pandoc -s -t man docs/system-ssh-agent.1.md | gzip > build/system-ssh-agent.1.gz

install: README.md src/system-ssh-agent.lib.sh src/env.sh build/system-ssh-agent.sh build/system-ssh-agent.service build/system-ssh-agent.1.gz
	install -d -m 755 \
		./build/buildroot/usr/local/share/doc/system-ssh-agent/ \
		./build/buildroot/usr/local/share/system-ssh-agent/  \
		./build/buildroot/usr/local/bin \
		./build/buildroot/etc/local/system-ssh-agent/\
		./build/buildroot/usr/local/share/man/man1/ \
		./build/buildroot/etc/systemd/system/ 
	install -m 644 src/system-ssh-agent.lib.sh ./build/buildroot/usr/local/share/system-ssh-agent/ 
	install -m 644 src/env.sh ./build/buildroot/etc/local/system-ssh-agent/
	install -m 644 README.md LICENSE ./build/buildroot/usr/local/share/doc/system-ssh-agent/
	install -m 755 build/system-ssh-agent.sh ./build/buildroot/usr/local/bin/
	install -m 644 build/system-ssh-agent.1.gz ./build/buildroot/usr/local/share/man/man1/
	install -m 644 build/system-ssh-agent.service ./build/buildroot/etc/systemd/system/ 

clean:
	rm -rf build
