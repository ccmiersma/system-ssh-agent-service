---
title: system-ssh-agent
author: Christopher Miersma
date: December 2017
section: 1
header: system-ssh-agent - Documentation 
footer: system-ssh-agent 0.2.x
---

# NAME

system-ssh-agent - manage files outside /etc by mirroring them into /etc

# SYNOPSIS

**system-ssh-agent.sh command**

# DESCRIPTION

This service allows you to start the ssh-agent for host or user keys and offload security from other `systemd` services
so that they can reliably use the system's private ssh keys rather than user keys. The primary benefit is the flexibility of managing keys.
It does not increase/decrease security of the system's already unencrypted keys.

This command would normally by run from a systemd service, but can be launched from the command line manually.

# COMMANDS

**start** This starts the ssh-agent in the background and writes the status to a file in /tmp

**stop** This stops the service.

**add-system-keys** This runs ssh-add for the private keys in /etc/ssh/

