%define author Christopher Miersma

Name:		system-ssh-agent-service
Version:        0.2.2
Release:        1.local%{?dist}

Summary:	System SSH Agent Service
Group:		Utilities
License:	MIT
URL:		https://gitlab.com/ccmiersma/%{name}/
Source0:	%{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  pandoc
BuildRequires:  ruby
BuildRequires:  rubygems

%description
This service allows you to start the ssh-agent for host or user keys and offload security from other systemd services
so that they can reliably use the system's private ssh keys rather than user keys. The primary benefit is the flexibility of managing keys.
It does not increase/decrease security of the system's already unencrypted keys.


%{!?local_prefix:%define local_prefix %(echo %{release} | cut -f2 -d. | egrep -v "(^el[[:digit:]]|^ol[[:digit:]]|^fc[[:digit:]])" )}


%if "%{local_prefix}" != "" && "%{local_prefix}" != "git"  
  %if "%{local_prefix}" == "opt"  
    %define _prefix /opt
  %else
    %define _prefix /opt/%{local_prefix}
  %endif
  %define _sysconfdir /etc/%{_prefix}
  %define _localstatedir /var/%{_prefix}
  %define _datadir %{_prefix}/share
  %define _docdir %{_datadir}/doc
  %define _mandir %{_datadir}/man
  %define _bindir %{_prefix}/bin
  %define _sbindir %{_prefix}/sbin
  %define _libdir %{_prefix}/lib
  %define _libexecdir %{_prefix}/libexec
  %define _includedir %{_prefix}/include
%endif


%prep
%setup


%build

./configure --prefix=%_prefix --sysconfdir=%_sysconfdir --localstatedir=%_localstatedir --destdir=%buildroot
make

%install


make install

#Manually defined files and dirs that need special designation.
#This will end up in the files section.
cat > %{name}-defined-files-list << EOF
%config(noreplace) %_sysconfdir/system-ssh-agent/env.sh
%docdir %{_mandir}
%docdir %{_docdir}
EOF
#Convoluted stuff to combine the manual list above with any new files we find, into a correct list with no duplicates
find ${RPM_BUILD_ROOT} -type f -o -type l | sed -e "s#${RPM_BUILD_ROOT}##g"|sed -e "s#\(.*\)#\"\1\"#" > %{name}-all-files-list
cat %{name}-defined-files-list | cut -f2 -d' ' | sed -e "s#\(.*\)#\"\1\"#" | sort > %{name}-defined-files-list.tmp
cat %{name}-all-files-list | sort > %{name}-auto-files-list.tmp
diff -e %{name}-defined-files-list.tmp %{name}-auto-files-list.tmp | grep "^\"" > %{name}-auto-files-list
cat %{name}-defined-files-list %{name}-auto-files-list > %{name}-files-list

%clean
%__rm -rf ${RPM_BUILD_ROOT}

%files -f %{name}-files-list
%defattr(-,root,root, -)


# The post and postun update the man page database
%post

mandb
systemctl daemon-reload

%postun

mandb
systemctl daemon-reload

%changelog
* Fri Dec 15 2017 Christopher Miersma <ccmiersma@gmail.com> 0.2.2-1.local
- Added CI config. (ccmiersma@gmail.com)

* Fri Dec 15 2017 Christopher Miersma <ccmiersma@gmail.com> 0.2.1-1.local
- new package built with tito

* Fri Dec 08 2017 Christopher Miersma <ccmiersma@gmail.com> 0.2.0-1.local
- new package built with tito



